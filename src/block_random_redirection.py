import tornado.ioloop
import tornado.web
import json

from pathlib import Path
from random_redirection_endpoint import RadomRedirection
from sys import stderr


def printError(*args, **kwargs):
    print(*args, **kwargs, file=stderr)


class BlockRandomRedirection:
    def __init__(self):
        self.DEFAULT_REDIRECTIONS = [
            {
                "note": None,
                "url": None
            }
        ]
        self.DEFAULT_PORT = 8888
        self.setupFromGlobalConfig()
        self.setupWebService()

    def setupFromGlobalConfig(self):
        try:
            f = open(str(Path.home()) + '/.blockRedirection.conf.json', encoding='utf-8')
            config = json.load(f)

            self.port = config.get('port', self.DEFAULT_PORT)
            self.redirections = config.get('redirections', self.DEFAULT_REDIRECTIONS)
            if len(self.redirections) == 0:
                self.redirections = self.DEFAULT_REDIRECTIONS
        except OSError:
            print('file ~/.blockRedirection.conf.json with configuration not found.\n')
            self.port = self.DEFAULT_PORT
            self.redirections = self.DEFAULT_REDIRECTIONS
        return self.redirections

    def setupWebService(self):
        self.app = tornado.web.Application([
            (r"/", RadomRedirection, dict(redirectionsList=self.redirections, updateDataCallback=self.setupFromGlobalConfig)),
        ])
        self.app.listen(8888)
        tornado.ioloop.IOLoop.current().start()