import tornado.web
import os
import random


class RadomRedirection(tornado.web.RequestHandler):
    def initialize(self, redirectionsList, updateDataCallback):
        self.redirectionsList = redirectionsList
        self.updateDataCallback = updateDataCallback

    def getRandom(self):
        return random.sample(self.redirectionsList, 1)

    def processElement(self, element):
        note = element.get('note', 'No url')
        url = element.get('url', None)
        if url != None:
            self.redirect(url)
        else:
            self.finish(note)

    def get(self):
        self.redirectionsList = self.updateDataCallback()
        randomRedirection = self.getRandom()
        self.processElement(randomRedirection[0])