#!/bin/bash
#Do not run script as administrator

if [ "$(id -u)" == "0" ]
then
  echo "Script cannot be run as administrator. Only insert password when required.";
  exit 1
fi

if ! command -v pip3 &> /dev/null
then
    echo "pip3 could not be found."
    exit 1
fi

# install necessary packages
pip3 install pyinstaller
pip3 install pathlib
pip3 install tornado

# create executable
pyinstaller src/blockRandomRedirection.py --onefile --hidden-import 'packaging.requirements'

# copy global config file to user home directory if not exist
if [ ! -f ~/.blockRedirection.conf.json ]
then
  cp .blockRedirection.conf.json ~/
else
  echo 'File .blockRedirection.conf.json already exist in user home directory.'
fi

# make file executable
chmod +x dist/blockRandomRedirection

# copy executable
sudo cp dist/blockRandomRedirection /usr/local/bin

# make executable run on startup
if [ ! -f ~/.config/systemd/user/blockRandomRedirection.service ]
then
  cp blockRandomRedirection.service ~/.config/systemd/user/
else
  echo 'Service for blockRandomRedirection is already created.'
fi

echo 'Wait 5 seconds or more and copy address bellow to your block site'
rm -r /tmp/ssh_localhost.run
ssh -n -R 80:localhost:8888 ssh.localhost.run > /tmp/ssh_localhost.run 2>/dev/null &
sleep 5

it=0
while [ ! -s /tmp/ssh_localhost.run ]
do
  sleep 1
  it=$((it + 1))
  if [ "$it" -gt 15 ]
  then
    echo 'Error on connection';
    echo 'Try yourself: ssh -R 80:localhost:8888 ssh.localhost.run'
    exit 1
  fi
done

echo "Here is your token now you can place it in browser: `cat /tmp/ssh_localhost.run | cut -d ' ' -f1`"
echo "Your address always will be in file: /tmp/ssh_localhost.run If connection succeed"

# enable service and start
systemctl --user enable blockRandomRedirection.service
systemctl --user start blockRandomRedirection.service