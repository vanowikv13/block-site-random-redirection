# BlockRandom Redirection
Projects let you redirect yourself to random address specified when trying to reach blocked sites.
The project works with extension: https://blocksite.co/.

## Installation guide (only for Linux with systemd manager)
Just run script install.sh **not as administrator** and insert password when necessary.

## Config files
After installation, path to config file is: ~/.blockRedirection.conf.json.
Program will reach the user home directory as a location so don't move a file.

Address of redirection will be always in a file: /tmp/ssh_localhost.run if on a startup we didn't have any errors on connection.